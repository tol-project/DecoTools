
//////////////////////////////////////////////////////////////////////////////
// GeneralizedDueTo.Period

//////////////////////////////////////////////////////////////////////////////
//BBL $GeneralizedDueTo.Period%es Devuelve una descomposición \
//BBL de tipo dueto generalizado que explica el total transformado \
//BBL de manera proporcional al dueto respecto a un periodo anterior \
//BBL del total sin transformar.
Set GeneralizedDueTo.Period(Set decomposition, Code transf, Real period)
//////////////////////////////////////////////////////////////////////////////
{
  Text grammar = _GetGrammar(decomposition);
  Set decomposition_B = Case(grammar=="Serie", {
    _GeneralizedDueTo.Period_Serie(decomposition, 
      transf, period)
  }, True, {
    WriteLn("[GeneralizedDueTo.Period] "<<BabelTool::Message(
      "#DecoTools$Deco.Serie/Error.OnlySeries", Empty), "E");
    Copy(Empty)
  });
  Set names = _GeneralizedDueTo.Period.GetNames(decomposition, 
    transf, period);
  _Rename(decomposition_B, names)
};

//////////////////////////////////////////////////////////////////////////////
Set _GeneralizedDueTo.Period.GetNames(Set decomposition, Code transf, 
  Real period)
//////////////////////////////////////////////////////////////////////////////
{
  Set names = _GetNames(decomposition);
  If(DueTo.Rename.Active, {
    Text transfName = If(Name(transf)!="", Name(transf), "Transf");
    Text periodName = If(period>0, "B"<<period, "F"<<(Real -period));
    Text decompName = transfName<<"."<<periodName;
    Text decName = names[1]<<"_DueTo."<<decompName;
    Set addNames = [[
      names[2][1]<<"_"<<transfName;
      names[2][1]<<"_"<<decompName
    ]] << For(2, Card(names[2]), Text (Real i) {
      names[2][i]<<"_DueTo."<<decompName
    });
    [[ decName, addNames ]]
  }, {
    Set addNames = [[names[2][1], "Reference"]]<<Remove(Copy(names[2]), 1);
    [[ names[1], addNames ]]
  })
};

//////////////////////////////////////////////////////////////////////////////
Set _GeneralizedDueTo.Period_Serie(Set decomposition, Code transf, 
  Real period)  
//////////////////////////////////////////////////////////////////////////////
{
  Serie total0 = decomposition[1];
  Serie total = transf(total0);
  Polyn polyn = If(period>0, 1 - B^period, 1 - F^(-period));
  Date first = Succ(First(total), Dating(total), Max(period,0));
  Date last = Succ(Last(total), Dating(total), Min(0,period));
  Serie ef.total = SubSer(total, first, last);
  Serie ef.base = SubSer((1-polyn):total, first, last);
  Serie ef.factor = (Serie polyn:total) / (Serie polyn:total0);
  Serie ef.factor := IfSer(IsUnknown(ef.factor), total, ef.factor);
  Set effects = For(2, Card(decomposition), Serie (Real i) {
    Serie effect = (Serie polyn:decomposition[i]) * ef.factor
  });
  Set decomposition.DueTo = [[ ef.total, ef.base ]] << effects
};

//////////////////////////////////////////////////////////////////////////////
