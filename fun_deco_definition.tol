
//////////////////////////////////////////////////////////////////////////////
//BBL $ShowGroupingTraces%es Muestra las trazas con la lista de \
//BBL contribuciones agrupadas.
Real ShowGroupingTraces = False;

//////////////////////////////////////////////////////////////////////////////
//BBL $@Contribution.Def%es Estructura que permite definir una nueva \
//BBL contribución como combinación de contribuciones de referencia.
//BBL Argumentos:
//BBL  * Text Name
//BBL  * SetOf{@Contribution.Def} Contributions
Struct @Contribution.Def
//////////////////////////////////////////////////////////////////////////////
{
  //BBL $@Contribution.Def::Name%es Nombre de la nueva contribución.
  Text Name;
  //BBL $@Contribution.Def::Contributions%es Conjunto de contribuciones \
  //BBL de referencia que conformarán la nueva contribución.
  Set Contributions
};

//////////////////////////////////////////////////////////////////////////////
//BBL $@Contribution.Ref%es Estructura que permite definir una contribución
//BBL de referencia.
//BBL Argumentos:
//BBL  * [Real|Text] Info
//BBL  * [Real|Serie|...|Code] BasePart
Struct @Contribution.Ref
//////////////////////////////////////////////////////////////////////////////
{
  //BBL $@Contribution.Ref::Info%es Nombre de la contribución de referencia \
  //BBL o su índice en la descomposición de referencia.
  Anything Info; // Real | Text
  //BBL $@Contribution.Ref::BasePart%es Parte que se sustrae a la \
  //BBL contribución de referencia a la hora de crear la nueva contribución.
  //BBL Puede ser un valor fijo o una función que lo obtenga al aplicarla \
  //BBL sobre la contribución.
  Anything BasePart  // Real | Serie | ... | Code
};

//////////////////////////////////////////////////////////////////////////////
//BBL $Deco.BaseDefinition%es Crea una nueva descomposición a partir de otra \
//BBL agrupando sus contribuciones según se indica en la nueva definición. 
//BBL La primera contribución nueva será una contribución base y asumirá la \
//BBL diferencia que pudiera haber entre el total y la suma del resto de \
//BBL contribuciones.
Set Deco.BaseDefinition(Set decomposition, Set definition)
//////////////////////////////////////////////////////////////////////////////
{
  Set baseDef = definition[1];
  Text baseName = baseDef->Name;
  Real If(Card(baseDef->Contributions)>0, {
    //BBL $Deco.BaseDefinition/Error.Base.Def%es La definición de la primera \
    //BBL contribución (base) no ha de tener contribuciones de referencia. \
    //BBL Éstas se ignorarán. 
    WriteLn("[Deco.BaseDefinition] "<<BabelTool::Message(
      "#DecoTools$Deco.BaseDefinition/Error.Base.Def", Empty), "W");
  0});
  Set new.contrs = SetConcat(For(2, Card(definition), Set (Real i) {
    Text name = definition[i]->Name;
    Real If(ShowGroupingTraces, {
      WriteLn("[Deco.BaseDefinition] C.Def ("<<(Real i)
        <<") '"<<name<<"'");
    1});
    Set contrRefs = definition[i]->Contributions;
    Set contrs = SetConcat(EvalSet(contrRefs, Set (Set contrRef) {
      Set indices = _FindContributions(decomposition, contrRef);
      EvalSet(indices, Anything (Real index) {
        Real If(ShowGroupingTraces, {
          Text cName = Name(decomposition[index+1]);
          WriteLn("[Deco.BaseDefinition] + C.Ref ("<<(Real index)
            <<") '"<<cName<<"'");
        1});
        Anything contr = decomposition[index+1];
        Anything basePart = contrRef->BasePart;
        If(Grammar(basePart)=="Code", {
          Code code = basePart;
          contr - code(contr)
        }, {
          contr - basePart
        })
      })
    }));
    If(Card(contrs), {
      Code setSum = FindCode(Grammar(contrs[1]), "SetSum");
      Anything new.contr = setSum(contrs);
      Set [[ _WithName(name, new.contr) ]]
    }, {
      Anything new.contr = decomposition[1] * 0;
      Set [[ _WithName(name, new.contr) ]]
    })
  }));
  Anything base = If(Card(new.contrs), 
    decomposition[1] - SetSum(new.contrs), decomposition[1]);
  [[ decomposition[1], _WithName(baseName, base) ]] << new.contrs
};

//////////////////////////////////////////////////////////////////////////////
Set _FindContributions(Set decomposition, @Contribution.Ref reference)
//////////////////////////////////////////////////////////////////////////////
{
  Anything info = reference->Info;
  Text grammar = Grammar(info);
  Case(grammar=="Real", {
    Real length = Card(decomposition)-1;
    If(info>0 & info<=length, [[info]], Copy(Empty))
  }, grammar=="Text", {
    Select(For(2, Card(decomposition), Real (Real i) {
      If(TextMatch(Name(decomposition[i]), info), i-1, 0)
    }), Real (Real x) { x })
  })
};

//////////////////////////////////////////////////////////////////////////////
Set Contributions.EditRefInfo_Text(Set contributions, Code editFunction)
//////////////////////////////////////////////////////////////////////////////
{
  EvalSet(contributions, Set (Set cDef) {
    @Contribution.Def(cDef->Name, EvalSet(cDef->Contributions, 
      Set (Set cRef) {
      Anything info = If(Grammar(cRef->Info)=="Real", cRef->Info, 
        editFunction(cRef->Info));
      @Contribution.Ref(info, cRef->BasePart)
    }))
  })
};

//////////////////////////////////////////////////////////////////////////////
