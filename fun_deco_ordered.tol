
//////////////////////////////////////////////////////////////////////////////
// Deco.Ordered

//////////////////////////////////////////////////////////////////////////////
//BBL $Deco.Ordered%es Devuelve la descomposición aditiva ordenada obtenida \
//BBL al aplicar la transformación.
Set Deco.Ordered(Set decomposition, Code transf)
//////////////////////////////////////////////////////////////////////////////
{
  Set decomposition_B = _GetGrammar.Method(decomposition,
    _Deco.Ordered_Real, [[transf]]);
  Set names = _Deco.Ordered.GetNames(decomposition, transf);
  _Rename(decomposition_B, names)
};

//////////////////////////////////////////////////////////////////////////////
Set _Deco.Ordered.GetNames(Set decomposition, Code transf)
//////////////////////////////////////////////////////////////////////////////
{
  Set names = _GetNames(decomposition);
  Real hasEffect0 = (_ZeroEffect(decomposition, transf)!=0);
  If(Deco.Rename.Active, {
    Text transfName = _GetNameIf(Name(transf), "Transf");

    Text decName = names[1]<<"_Ordered";
    Set addNames = [[
      names[2][1]<<"_"<<transfName
    ]] << For(2-hasEffect0, Card(names[2]), Text (Real i) {
      names[2][i]<<"_Effect."<<(Real i-1)
    });
    [[ decName, addNames ]]
  }, {
    [[ names[1], Set [[names[2][1] ]]<<If(hasEffect0, [["Effect0"]], Empty)
      <<Remove(Copy(names[2]),1) ]]
  })
};

//////////////////////////////////////////////////////////////////////////////
Set _Deco.Ordered_Real(Set decomposition, Code transf)
//////////////////////////////////////////////////////////////////////////////
{
  Real Test(decomposition);
  Real length = Card(decomposition);
  Anything total = decomposition[1];
  Anything base = total * 0;
  Set contributions = ExtractByIndex(decomposition, Range(2, length, 1));
  Real ef.total = transf(total);
  Real ef.base = transf(base);
  Anything total_agg = base;
  Set effects = EvalSet(contributions, Real (Anything contribution) {
    Real effect = transf(total_agg + contribution) - transf(total_agg);
    Anything total_agg := total_agg + contribution;
    effect
  });
  [[ ef.total ]] << If(ef.base, [[ ef.base ]], Copy(Empty)) << effects
};

//////////////////////////////////////////////////////////////////////////////
// Deco.BaseOrdered

//////////////////////////////////////////////////////////////////////////////
//BBL $Deco.BaseOrdered%es Devuelve la descomposición aditiva ordenada con \
//BBL contribución base obtenida al aplicar la transformación.
Set Deco.BaseOrdered(Set decomposition, Code transf)
//////////////////////////////////////////////////////////////////////////////
{
  Set decomposition_B = _GetGrammar.Method(decomposition,
    _Deco.BaseOrdered_Real, [[transf]]);
  Set names = _Deco.BaseOrdered.GetNames(decomposition, transf);
  _Rename(decomposition_B, names)
};

//////////////////////////////////////////////////////////////////////////////
Set _Deco.BaseOrdered.GetNames(Set decomposition, Code transf)
//////////////////////////////////////////////////////////////////////////////
{
  Set names = _GetBaseNames(decomposition);
  If(Deco.Rename.Active, {
    Text transfName = _GetNameIf(Name(transf), "Transf");
    Text decName = names[1]<<"_BaseOrdered";
    Set addNames = [[
      names[2][1]<<"_"<<transfName;
      names[2][2]<<"_"<<transfName
    ]] << For(3, Card(names[2]), Text (Real i) {
      names[2][i]<<"_BaseEffect."<<(Real i-2)
    });
    [[ decName, addNames ]]
  }, names)
};

//////////////////////////////////////////////////////////////////////////////
Set _Deco.BaseOrdered_Real(Set decomposition, Code transf)
//////////////////////////////////////////////////////////////////////////////
{
  Real Test(decomposition);
  Real length = Card(decomposition);
  Anything total = decomposition[1];
  Anything base = decomposition[2];
  Set contributions = ExtractByIndex(decomposition, Range(3, length, 1));
  Real ef.total = transf(total);
  Real ef.base = transf(base);
  Anything total_agg = base;
  Set effects = EvalSet(contributions, Real (Anything contribution) {
    Real effect = transf(total_agg + contribution) - transf(total_agg);
    Anything total_agg := total_agg + contribution;
    effect
  });
  [[ ef.total, ef.base ]] << effects
};

//////////////////////////////////////////////////////////////////////////////
